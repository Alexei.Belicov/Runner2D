﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace PlayerScripts
{
    public class Player : MonoBehaviour
    {
        public event UnityAction<int> HealthChangedEvent;
        public event UnityAction DiedEvent;
        
        [SerializeField] private int _health;

        private void Start()
        {
            HealthChangedEvent?.Invoke(_health);
        }

        public void ApplyDamage(int damage)
        {
            _health -= damage;
            HealthChangedEvent?.Invoke(_health);
            
            if (_health <= 0)
                Die();
        }

        private void Die()
        {
            DiedEvent?.Invoke();
        }
    }
}