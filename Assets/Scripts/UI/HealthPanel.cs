﻿using System;
using PlayerScripts;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UI
{
    public class HealthPanel : MonoBehaviour
    {
        [SerializeField] private Player _player;
        [SerializeField] private Image _healthImage;
        [SerializeField] private TMP_Text _healthText;

        private void OnEnable()
        {
            _player.HealthChangedEvent += OnHealthChangedEvent;
        }

        private void OnDisable()
        {
            _player.HealthChangedEvent -= OnHealthChangedEvent;
        }
        
        private void OnHealthChangedEvent(int health)
        {
            _healthText.text = health.ToString();
        }

        public void DisableHealthDisplay()
        {
            _healthText.alpha = 0f;
            _healthImage.enabled = false;
        }
    }
}
