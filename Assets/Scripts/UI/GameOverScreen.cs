﻿using PlayerScripts;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class GameOverScreen : MonoBehaviour
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Player _player;
        [SerializeField] private Spawner _spawner;
        [SerializeField] private HealthPanel _healthPanel;
        
        private CanvasGroup _gameOverGroup;
        
        private void OnEnable()
        {
            _player.DiedEvent += OnDiedEvent;
            _restartButton.onClick.AddListener(OnRestartButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
        }

        private void Start()
        {
            _gameOverGroup = GetComponent<CanvasGroup>();
            _gameOverGroup.alpha = 0f;
        }
        
        private void OnDisable()
        {
            _player.DiedEvent -= OnDiedEvent;
            _restartButton.onClick.RemoveListener(OnRestartButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
        }
        
        private void OnDiedEvent()
        {
            _gameOverGroup.alpha = 1f;
            Time.timeScale = 0f;
            _healthPanel.DisableHealthDisplay();
            _player.gameObject.SetActive(false);
            _spawner.gameObject.SetActive(false);
            
        }

        private void OnRestartButtonClick()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneBuildIndex: 0);
        }
        
        private void OnExitButtonClick()
        {
            Application.Quit();
        }
    }
}